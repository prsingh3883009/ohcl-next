import { AreaChartOutlined, BarChartOutlined } from "@ant-design/icons";
import type { MenuProps } from "antd";
import { Layout, Menu, theme } from "antd";
import React, { Key, useState } from "react";
import CandleStick from "./charts/candleStick";
import Orderbook from "./charts/OrderBook";
import BitfinexTicker from "./websockets/BitFinexTicker";

const { Header, Content, Footer, Sider } = Layout;

type MenuItem = Required<MenuProps>["items"][number];

function getItem(
  label: React.ReactNode,
  key: React.Key,
  icon?: React.ReactNode
  //   children?: MenuItem[]
): MenuItem {
  return {
    key,
    icon,
    // children,
    label,
  } as MenuItem;
}

const items: MenuItem[] = [
  getItem("OHCL Chart", "1", <BarChartOutlined />),
  getItem("Order Book", "2", <AreaChartOutlined />),
];

//--------------------------------- return statement ----------------------------------------//

const App: React.FC = () => {
  const [collapsed, setCollapsed] = useState(false);
  const [selectedMenuItem, setSelectedMenuItem] = useState<Key>("1");
  const handleMenuClick = (e: MenuItem) => {
    if (e && e.key) setSelectedMenuItem(e.key);
  };
  const {
    token: { colorBgContainer },
  } = theme.useToken();

  return (
    <Layout style={{ minHeight: "100vh" }}>
      <Sider
        collapsible
        collapsed={collapsed}
        onCollapse={(value) => setCollapsed(value)}
      >
        <div
          style={{
            height: 32,
            margin: 16,
            background: "rgba(255, 255, 255, 0.2)",
          }}
        />
        <Menu
          theme="dark"
          defaultSelectedKeys={["1"]}
          mode="inline"
          items={items}
          onClick={(e) => handleMenuClick(e)}
        />
      </Sider>
      <Layout className="site-layout">
        <Content style={{ margin: "0 16px" }}>
          {selectedMenuItem === "1" ? (
            <div
              style={{
                padding: 24,
                minHeight: 360,
                background: colorBgContainer,
              }}
            >
              <CandleStick />
            </div>
          ) : (
            <div
              style={{
                padding: 24,
                minHeight: 360,
                background: colorBgContainer,
              }}
            >
              <BitfinexTicker/>
              <Orderbook />
            </div>
          )}
        </Content>
      </Layout>
    </Layout>
  );
};

export default App;
