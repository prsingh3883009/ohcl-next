import Table from 'react-bootstrap//Table';

const Orderbook = (props: any) => {

  const renderBidRows = () => {
    return props?.orderBookBids?.map((row: any, index: any) =>
    (<tr className="bids-row" key={row.price}>
      {props.type === 'asks' ? <td style={{
        display: (index > 23 ? 'none' : 'initial'),
        position: 'absolute', left: '0px', background: 'rgba(139,42,2, 0.3)',
        width: `calc(${Number((100 * row.total / props.orderBookBids[props.orderBookBids.length - 1].total).toFixed(0)) * 0.7}% - 20px)`,
        height: '15px'
      }}
      ></td> : <td style={{
        display: (index > 23 ? 'none' : 'initial'),
        position: 'absolute', right: '0px', background: 'rgba(82,108,46, 0.3)',
        width: `calc(${Number((100 * row.total / props.orderBookBids[props.orderBookBids.length - 1].total).toFixed(0)) * 0.7}% - 20px)`,
        height: '15px'
      }}
      ></td>}
      <td 
      style={{
        height: '15px'
      }}
        className='text-center'>{row.count}</td>
      <td className='text-right'>{row.amount}</td>
      <td className='text-right'>{row.total}</td>
      <td className='text-right'>{row.price}</td>
    </tr>)
    );
  }

  return (
    <Table style={{ maxHeight: '50vh', minWidth: '100%' }}>
      <thead>
        <tr>
          <th></th>
          <th className='text-center'>COUNT</th>
          <th className='text-right'>AMOUNT</th>
          <th className='text-right'>TOTAL</th>
          <th className='text-right'>PRICE</th>
        </tr>
      </thead>
      <tbody>
        {renderBidRows()}
      </tbody>
    </Table>
  );
};

export default Orderbook;
