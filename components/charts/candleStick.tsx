import preProcessData from "@/utils/dataProcessor";
import ReactEcharts from "echarts-for-react";
// import echarts from 'echarts/dist/echarts';
import { useEffect, useState } from "react";
import TickersSelect from "../selectDropDowns/TickersSelect";
import TimeFrameSelect from "../selectDropDowns/TimeFrameSelect";
import axios from 'axios';
import { createConnection } from "../websockets/bitfinex";


const OhclChart: React.FC = () => {
  //---------------------------- all states --------------------------------//
  const [apiData, setApiData] = useState<number[][]>([]);
  const [data, setData] = useState<number[][]>([]);
  const [timeData, setTimeData] = useState<string[]>([]);
  const [startTime, setStartTime] = useState<number>(
    new Date().setTime(new Date().getHours() - 1)
  );
  const [queryTimeFrame, setQueryTimeFrame] = useState<String>("1m");
  const [selectedTicker, setSelectedTicker] = useState<String>('tBTCUSD');

  // -------------------- fetching candles api for data ----------------------------//
  const fetchApi = async () => {
    const pathParams = `candles/trade:${queryTimeFrame}:${selectedTicker}/hist`;
    const currentTimeInMilliseconds = new Date().getTime();
    let queryParams = startTime
      ? `start=${startTime}&end=${currentTimeInMilliseconds}&limit=330`
      : `end=${currentTimeInMilliseconds}&limit=330`;

      const initialData = await axios.get(`/api/candle?pathname=${pathParams}&${queryParams}`);
      setApiData(initialData.data)
  };

  // ----------------------- initial api call on page load --------------------------//
  useEffect(() => {
    fetchApi();
  }, []);

  // --------------- setting data for chart on every change of data from api -------//
  useEffect(() => {
    if (apiData && apiData.length > 0) {
      const { processedData, timeArray } = preProcessData(apiData);
      setData([...processedData]);
      setTimeData(timeArray);
    }
  }, [apiData]);

  const handleClick = () =>{
    console.log('from handle click')
  }

  // ---------------------- setting config options for chart -----------------------//
  const getOption = () => {
    return {
      legend:{
        type: "plain",
        id:"random",
        show: true
      },
      xAxis: {
        type: "category",
        data: timeData,
        zoomLock: true,
        zoomOnMouseWheel: true,
      },
      yAxis: {
        type: "value",
        scale: true,
        zoomLock: true,
        zoomOnMouseWheel: true,
      },
      dataZoom: [
        // Add a data zoom component to enable zooming on the chart
        {
          type: "inside",
          xAxisIndex: [0],
          start: 20,
          end: 100,
        },
      ],
      series: [
        {
          type: "candlestick",
          data,
        },
      ],
    };
  };

  // ------------------------- calling api on change of start time exchange symbol change ----------------//
  useEffect(() => {
    if (startTime) fetchApi();
  }, [startTime, selectedTicker]);

  // ------------------------- return statement -----------------------------------//
  return (
    <>
      <ReactEcharts 
        option={getOption()}
        // echarts={echarts}
        style={{ height: '500px', width: '100%' }}
      />
      <TimeFrameSelect
        setStartTime={setStartTime}
        setQueryTimeFrame={setQueryTimeFrame}
      />
      <TickersSelect setSelectedTicker={setSelectedTicker}/>
    </>
  );
};

export default OhclChart;
