import getStartTime from "@/utils/TimeFrame";
import { Select } from "antd";
import { Dispatch, SetStateAction } from "react";

const SELECT_OPTIONS = [
  { value: "3y", label: "3Y" },
  { value: "1y", label: "1Y" },
  { value: "3m", label: "3M" },
  { value: "1m", label: "1M" },
  { value: "7d", label: "7D" },
  { value: "3d", label: "3D" },
  { value: "1d", label: "1D" },
  { value: "6h", label: "6hr" },
  { value: "1h", label: "1hr" },
];

interface Props {
  setStartTime: Dispatch<SetStateAction<number>>;
  setQueryTimeFrame: Dispatch<SetStateAction<String>>;
}

const TimeFrameSelect = ({ setStartTime, setQueryTimeFrame }: Props) => {
  return (
    <Select
      defaultValue="1h"
      style={{ width: 120 }}
      onChange={(e) => {
        console.log(e);
        const { startTimeInMilliseconds, timeFrame } = getStartTime(e);
        setStartTime(startTimeInMilliseconds);
        setQueryTimeFrame(timeFrame);
      }}
      options={SELECT_OPTIONS}
    />
  );
};

export default TimeFrameSelect;
