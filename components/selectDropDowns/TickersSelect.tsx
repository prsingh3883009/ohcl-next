import { Select } from "antd";
import { Dispatch, SetStateAction, useEffect, useState } from "react";
import axios from 'axios'

interface Props {
  setSelectedTicker: Dispatch<SetStateAction<String>>;
}

const TickersSelect = ({ setSelectedTicker }: Props) => {
  const [allSymbols, setAllSymbols] = useState<Object[]>([]);

  //------------------------- setting options in select ---------------------------//
  const getAllSymbols = (data: string[]): Object[] => {
    const symbolsArray: Object[] = [];
    if (data && data.length > 0) {
      data.forEach((entry) =>
        symbolsArray.push({ value: entry[0], lable: entry[0] })
      );
    }
    return symbolsArray;
  };

  //---------------------- calling api for tickers here ---------------------------//
  const fetchApi = async () => {
    const data = await axios.get('/api/tickers');
    const response = data.data;
    const selectOptions = getAllSymbols(response);
    setAllSymbols([...selectOptions]);
  }



  //-------------------------- api calling for all tickers symbols ---------------//
  useEffect(() => {
    fetchApi();
  }, []);

  //------------------------------------- return statement ------------------------------//
  return (
    <Select
      defaultValue="tBTCUSD"
      style={{ width: 120 }}
      onChange={(e) => {
        setSelectedTicker(e);
      }}
      options={allSymbols}
    />
  );
};

export default TickersSelect;
