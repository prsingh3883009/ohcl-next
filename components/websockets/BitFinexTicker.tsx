import { Row, Col } from 'antd'
import { SetStateAction, useEffect, useState } from "react";
// import { createConnection } from "./bitfinex";
import Sockette from "sockette";
import OrderBook from '../charts/OrderBook'


interface Books {
  price?: number,
  count?: number,
  amount?: number,
  total?: number
}

// type PayLoadData {} || []
function BitfinexTicker() {
  // const [state, setState] = useState([]);
  const [bidOrderBook, setBidOrderBook] = useState<Books[]>([]);
  const [asksOrderBook, setAsksOrderBook] = useState<Books[]>([]);
  const [updatedBidsOrderBook, updateBidsOrderBook] = useState<Books>({});
  const [updatedAskOrderBook, updateAskOrderBook] = useState<Books>({});


  const handleUpdate = (existingState: Books[], updatedState: Books, stateToUpdate: { (value: SetStateAction<Books[]>): void; (arg0: Books[]): void; }) => {
    let priceExists = false;
    if (Object.keys(updatedState).length) {
      console.log(updatedState, 'bids updated');
      if (existingState.length > 0) {
        existingState.map(function (row, index) {
          if (existingState[index]?.price && updatedState.price && (existingState[index]?.price === updatedState.price)) {
            //Price already exists, so it should only update current row.
            priceExists = true;
            existingState[index].count = Number(updatedState.count?.toFixed(2));
            existingState[index].amount = Number(updatedState.amount?.toFixed(2));
            // state[index].total = (state[index - 1] ? state[index - 1].total : 0) + newState.amount;
          }
        })
      }

      if (priceExists) {
        priceExists = false;
        existingState.slice()
      }
      else {
        let newStateCombined: Books[] = [...existingState, updatedState];
        //Sort form highest to lowest price before returning.
        newStateCombined.sort((a, b) => b.price as number - (a.price as number));

        if (newStateCombined.length > 20) {
          newStateCombined.pop();
        }

        if (newStateCombined.length > 0) {
          newStateCombined.map(function (row, index) {
            if (!row || row.count === 0) {
              newStateCombined.splice(index, 1);
            }
          });

          newStateCombined.length && newStateCombined.map(function (row, index) {
            if (newStateCombined[index - 1] && newStateCombined[index - 1].total && row.amount) {
              newStateCombined[index].total = Number((newStateCombined[index - 1]?.total as number + row.amount).toFixed(2));
            }
            else {
              newStateCombined[index].total = Number(row.amount?.toFixed(2));
            }
          });
        }

        stateToUpdate(newStateCombined);
        // return newStateCombined;
      }
    }
  }

  useEffect(() => {
    // update bids order book
    handleUpdate(bidOrderBook, updatedBidsOrderBook, setBidOrderBook)
  }, [updatedBidsOrderBook]);

  useEffect(() => {
    // update asks order book
    handleUpdate(asksOrderBook, updatedAskOrderBook, setAsksOrderBook);
  }, [updatedAskOrderBook]);


  const createConnection = () => {
    let payloadData: Object | Array<Number> = {};
    const onConnectionEstablished = () => {
      ws.send(
        JSON.stringify({
          event: "subscribe",
          channel: "book",
          symbol: "tBTCUSD",
          prec: "P0",
          freq: "F0",
          len: 25,

        })
      );
    };
    const onMessageRecieved = (e: any) => {
      payloadData = JSON.parse(e.data)
      // console.log(payloadData, "from component", typeof(payloadData));
      if (
        Array.isArray(payloadData) &&
        Array.isArray(payloadData[1]) &&
        payloadData[1].length === 3
      ) {
        const tempBookOrder = {
          price: payloadData[1][0],
          count: payloadData[1][1],
          amount: payloadData[1][2],
          total: 0
        }
        if (tempBookOrder.amount > 0) {
          updateBidsOrderBook(tempBookOrder);
        } else {
          updateAskOrderBook(tempBookOrder);
        }

      };

      // return e.data;
    };
    const onConnectionClosed = (e: any) => {
      console.log('closed');
      console.log(e);
    }
    const ws = new Sockette('wss://api.bitfinex.com/ws/2', {
      timeout: 5e3,
      maxAttempts: 10,
      onopen: onConnectionEstablished,
      onmessage: onMessageRecieved,
      onreconnect: e => console.log('Reconnecting...', e),
      onmaximum: e => console.log('Stop Attempting!', e),
      onclose: e => onConnectionClosed,
      onerror: e => console.log('Error:', e)
    })
  }
  useEffect(() => {
    createConnection();
  }, []);

  return (
    <Row>
      <Col span={12}>
        <OrderBook orderBookBids={bidOrderBook} />
      </Col>

      <Col span={12}>
        <OrderBook orderBookBids={asksOrderBook} type='asks'/>
      </Col>
    </Row>
  );
}

export default BitfinexTicker;
