
import Sockette from "sockette";

// subscribe to ticker data for BTC/USD

export function createConnection() {
  // const ws = new WebSocket("wss://api-pub.bitfinex.com/ws/2");
  const onConnectionEstablished = () => {
    ws.send(
      JSON.stringify({
        event: "subscribe",
        channel: "book",
        symbol: "tBTCUSD",
        prec: "P0",
        freq: "F0",
        len: 25,

      })
    );
  };
  const onMessageRecieved = (e) => {
    console.log(e, "$$$$$$$$$$$$");
    return e.data;
  };
  const ws = new Sockette('wss://api.bitfinex.com/ws/2', {
      timeout: 5e3,
      maxAttempts: 10,
      onopen: onConnectionEstablished,
      onmessage: onMessageRecieved,
      onreconnect: e => console.log('Reconnecting...', e),
      onmaximum: e => console.log('Stop Attempting!', e),
      onclose: e => onConnectionClosed,
      onerror: e => console.log('Error:', e)
    })
  return ws;
}
