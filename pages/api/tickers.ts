import type { NextApiRequest, NextApiResponse } from 'next';
import axios from 'axios';

type Data = {
    name: string
}

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<Data>
) {
    //------------------ calling cross origin api ---------------//
    const options = { method: "GET", headers: { accept: "application/json" } };
    const initialData = await axios.get("https://api-pub.bitfinex.com/v2/tickers?symbols=ALL", options);
    res.status(200).json(initialData.data);
}
