import type { NextApiRequest, NextApiResponse } from 'next';
import axios from 'axios';

const BASE_URL = 'https://api-pub.bitfinex.com/v2'

type Data = {
  name: string
}

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {

  const { pathname, start, end, limit } = req.query;

  const initialData = axios.get(`${BASE_URL}/${pathname}?start=${start}&end=${end}&limit=${limit}`);
  res.status(200).send((await initialData).data)
}
