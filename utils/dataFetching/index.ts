const fetchApi = (): number[][] => {
  const startTime = new Date("01/02/2023").getTime();
  const pathParams = "candles/trade:1D:tBTCUSD/hist";
  const currentTimeInMilliseconds = new Date().getTime();
  let queryParams = startTime
    ? `start=${startTime}&end=${currentTimeInMilliseconds}&limit=330`
    : `end=${currentTimeInMilliseconds}&limit=330`;

  fetch(`https://api-pub.bitfinex.com/v2/${pathParams}?${queryParams}`)
    .then((response) => {
      return response.json();
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      console.log(err);
      return [];
    });
  return [];
};

export default fetchApi;
