const preProcessData = (data: number[][]) => {
  const processedData:number[][] = [];

  const timeArray: string[] = []
  if (data && data.length > 0) {
    data.forEach((set) => {
      timeArray.unshift(new Date(set[0]).toDateString());
      const dummyArray = [set[1], set[2], set[4], set[3]]
      processedData.unshift(dummyArray)

    });
  }
  return {processedData,timeArray };
};

export default preProcessData;
