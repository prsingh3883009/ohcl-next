const getStartTime = (timeframe: string) => {
  let startTime = new Date();
  let timeFrame = "1m";
  switch (timeframe) {
    case "3y":
      startTime.setFullYear(new Date().getFullYear() - 3);
      timeFrame = "1D";
      break;
    case "1y":
      startTime.setFullYear(new Date().getFullYear() - 1);
      timeFrame = "1D";
      break;
    case "3m":
      startTime.setMonth(startTime.getMonth() - 3);
      timeFrame = "1D";
      break;
    case "1m":
      startTime.setMonth(startTime.getMonth() - 1);
      timeFrame = "12h";
      break;
    case "7d":
      startTime.setDate(startTime.getDate() - 7);
      timeFrame = "1h";
      break;
    case "3d":
      startTime.setDate(startTime.getDate() - 3);
      timeFrame = "30m";
      break;
    case "1d":
      startTime.setDate(startTime.getDate() - 1);
      timeFrame = "15m";
      break;
    case "6h":
      startTime.setHours(startTime.getHours() - 6);
      timeFrame = "5m";
    case "1h":
      startTime.setHours(startTime.getHours() - 1);
      timeFrame = "1m";
  }
  const startTimeInMilliseconds = startTime.getTime();
  return { startTimeInMilliseconds, timeFrame };
};

export default getStartTime;
